// Lights LED's systematically according to the time taken from a RTC chip.
// Uses shift registers to control multiple LED's, and a push button to control time 
// according to daylight saving measures. Created by Jacob Unwin, released for free use,
// see http://www.jacob-unwin.com for more

//!!!!!!DON'T USE, JUST MESSES IT UP!!!!!!

#include <Wire.h>

#include "RTClib.h"

//How many of the shift registers - change this
#define number_of_74hc595s 4 

//defines the number of shift register pins - don't change this
#define numOfRegisterPins number_of_74hc595s * 8

boolean registers[numOfRegisterPins];

RTC_DS1307 RTC;

int roundMinute = 0;
int statedHour = 0;


int dsButton = 3;
int dsButtonState = 0; //holds the current state of the button
int dsButtonVal = 0; //holds the buttons current value
int dsButtonOldVal = 0; //holds the buttons previous value
int dsButtonPush = 2;


int SER_Pin = 8;   //pin 14 on the 75HC595
int RCLK_Pin = 9;  //pin 12 on the 75HC595
int SRCLK_Pin = 10; //pin 11 on the 75HC595

int shouldBeMinute = 0;
int shouldBeHour = 0;

unsigned long currentMillis = 0;
unsigned long previousMillis = 0;


const int power = 7; //connect the LED power source to pin 7

#include "Wire.h"
#define DS1307_I2C_ADDRESS 0x68  // This is the I2C address
#if defined(ARDUINO) && ARDUINO >= 100   // Arduino 1.0 and newer
  #define I2C_WRITE Wire.write 
  #define I2C_READ Wire.read
#else                                   // Arduino below 1.0 
  #define I2C_WRITE Wire.send 
  #define I2C_READ Wire.receive
#endif

byte zero;

 
// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return ((val/10*16) + (val%10) );
}


void setup () {
    Serial.begin(57600);
    Wire.begin();
    RTC.begin();
      zero=0x00;
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled according to the computer clock
    //RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  
  pinMode(SER_Pin, OUTPUT);
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);
  pinMode(power, OUTPUT);
  
  RTC.adjust(DateTime(__DATE__, __TIME__));
  
  delay(500);

  previousMillis = millis();

  //reset all register pins
  clearRegisters();
  writeRegisters();
}


//===========================loop======================

void loop () {
    DateTime now = RTC.now();
    
    currentMillis = millis();
    
    if(currentMillis - previousMillis > 6000){
      
      previousMillis = currentMillis;
      
      shouldBeMinute = now.minute() + 1;
      if (shouldBeMinute > 60){
        shouldBeMinute = 1;
      }
      
      shouldBeHour = now.hour() + 1;
      if (shouldBeHour > 24){
        shouldBeHour = 1;
      }
    }

  checkTime();
   
      
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
    
    Serial.print(" since midnight 1/1/1970 = ");
    Serial.print(now.unixtime());
    Serial.print("s = ");
    Serial.print(now.unixtime() / 86400L);
    Serial.println("d");
    
    // calculate a date which is 7 days and 30 seconds into the future
    DateTime future (now.unixtime() + 7 * 86400L + 30);
    
    Serial.print(" now + 7d + 30s: ");
    Serial.print(future.year(), DEC);
    Serial.print('/');
    Serial.print(future.month(), DEC);
    Serial.print('/');
    Serial.print(future.day(), DEC);
    Serial.print(' ');
    Serial.print(future.hour(), DEC);
    Serial.print(':');
    Serial.print(future.minute(), DEC);
    Serial.print(':');
    Serial.print(future.second(), DEC);
    Serial.println();
    
   
    
    //this block rounds the minute variable to the nearest 5 minutes
  //for use in the minute ring
  if ((now.minute() >= 3) && (now.minute() <= 7)){ 
    roundMinute = 5;    
  } else if ((now.minute() >= 8) && (now.minute() <= 12)){
    roundMinute = 10;    
  } else if ((now.minute() >= 13) && (now.minute() <= 17)){
    roundMinute = 15;    
  } else if ((now.minute() >= 18) && (now.minute() <= 22)){
    roundMinute = 20;    
  } else if ((now.minute() >= 23) && (now.minute() <= 27)){
    roundMinute = 25;    
  } else if ((now.minute() >= 28) && (now.minute() <= 32)){
    roundMinute = 30;    
  } else if ((now.minute() >= 33) && (now.minute() <= 37)){
    roundMinute = 35;    
  } else if ((now.minute() >= 38) && (now.minute() <= 43)){
    roundMinute = 40;    
  } else if ((now.minute() >= 44) && (now.minute() <= 47)){
    roundMinute = 45;    
  } else if ((now.minute() >= 48) && (now.minute() <= 53)){
    roundMinute = 50;    
  } else if ((now.minute() >= 54) && (now.minute() <= 57)){
    roundMinute = 55;    
  } else if ((now.minute() == 1) || (now.minute() == 2) || (now.minute() == 58) || (now.minute() == 59) || (now.minute() == 60) || (now.minute() == 0)){
    roundMinute = 60;  
  }
   
   Serial.print(" Round minute: ");
   Serial.println(roundMinute);
   Serial.print(" Stated hour: ");
   Serial.println(statedHour);
   Serial.println();

minuteLight(); //starts the minute ring control
hourLight(); //starts the hour ring control

  delay(1000);
    
}










//set all register pins to HIGH
void clearRegisters(){
  for(int i = numOfRegisterPins - 1; i >=  0; i--){
     registers[i] = HIGH;
  }
} 

//Set and display registers
//Only call AFTER all values are set how you would like (slow otherwise)
void writeRegisters(){

  digitalWrite(RCLK_Pin, LOW);

  for(int i = numOfRegisterPins - 1; i >=  0; i--){
    digitalWrite(SRCLK_Pin, LOW);

    int val = registers[i];

    digitalWrite(SER_Pin, val);
    digitalWrite(SRCLK_Pin, HIGH);

  }
  digitalWrite(RCLK_Pin, HIGH);

}



//set an individual pin HIGH or LOW
void setRegisterPin(int index, int value){
  registers[index] = value;
}

void minuteLight(){ //controls the minute ring

if (roundMinute == 60){
  clearMinuteRegisters();
  setRegisterPin(17, LOW);
  writeRegisters();
}     
if (roundMinute == 5){
  clearMinuteRegisters();
  setRegisterPin(18, LOW);
  writeRegisters();
}
if (roundMinute == 10){
  clearMinuteRegisters();
  setRegisterPin(19, LOW);
  writeRegisters();
}
if (roundMinute == 15){
  clearMinuteRegisters();
  setRegisterPin(20, LOW);
  writeRegisters();
}
if (roundMinute == 20){
  clearMinuteRegisters();
  setRegisterPin(21, LOW);
  writeRegisters();
}
if (roundMinute == 25){
  clearMinuteRegisters();
  setRegisterPin(22, LOW);
  writeRegisters();
}
if (roundMinute == 30){
  clearMinuteRegisters();
  setRegisterPin(25, LOW);
  writeRegisters();
}
if (roundMinute == 35){
  clearMinuteRegisters();
  setRegisterPin(26, LOW);
  writeRegisters();
}
if (roundMinute == 40){
  clearMinuteRegisters();
  setRegisterPin(27, LOW);
  writeRegisters();
}
if (roundMinute == 45){
  clearMinuteRegisters();
  setRegisterPin(28, LOW);
  writeRegisters();
}
if (roundMinute == 50){
  clearMinuteRegisters();
  setRegisterPin(29, LOW);
  writeRegisters();
}
if (roundMinute == 55){
  clearMinuteRegisters();
  setRegisterPin(30, LOW);
  writeRegisters();
}  
 
  
  delay(10);
}

void clearMinuteRegisters(){ //turns off all LED's in the minute Ring
  setRegisterPin(17 , HIGH);
  setRegisterPin(18 , HIGH);
  setRegisterPin(19 , HIGH);
  setRegisterPin(20 , HIGH);
  setRegisterPin(21 , HIGH);
  setRegisterPin(22 , HIGH);
  setRegisterPin(23 , HIGH);
  setRegisterPin(25 , HIGH);
  setRegisterPin(26 , HIGH);
  setRegisterPin(27 , HIGH);
  setRegisterPin(28 , HIGH);
  setRegisterPin(29 , HIGH);
   setRegisterPin(30 , HIGH);
  
  delay(10);
}

void hourLight(){ //controls the hour ring
  if ((statedHour == 12)||(statedHour == 24)){
  clearHourRegisters();
  setRegisterPin(15, LOW);
  writeRegisters();
}
  if ((statedHour == 1)||(statedHour == 13)){
    clearHourRegisters();
  setRegisterPin(2, LOW);
  writeRegisters();
}  if ((statedHour == 2)||(statedHour == 14)){
  clearHourRegisters();
  setRegisterPin(3, LOW);
  writeRegisters();
}  if ((statedHour == 3)||(statedHour == 15)){
  clearHourRegisters();
  setRegisterPin(4, LOW);
  writeRegisters();
}  if ((statedHour == 4)||(statedHour == 16)){
  clearHourRegisters();
  setRegisterPin(5, LOW);
  writeRegisters();
}  if ((statedHour == 5)||(statedHour == 17)){
  clearHourRegisters();
  setRegisterPin(6, LOW);
  writeRegisters();
}  if ((statedHour == 6)||(statedHour == 18)){
  clearHourRegisters();
  setRegisterPin(9, LOW);
  writeRegisters();
}  if ((statedHour == 7)||(statedHour == 19)){
  clearHourRegisters();
  setRegisterPin(10, LOW);
  writeRegisters();
}  if ((statedHour == 8)||(statedHour == 20)){
  clearHourRegisters();
  setRegisterPin(11, LOW);
  writeRegisters();
}  if ((statedHour == 9)||(statedHour == 21)){
  clearHourRegisters();
  setRegisterPin(12, LOW);
  writeRegisters();
}  if ((statedHour == 10)||(statedHour == 22)){
  clearHourRegisters();
  setRegisterPin(13, LOW);
  writeRegisters();
}  if ((statedHour == 11)||(statedHour == 23)){
  clearHourRegisters();
  setRegisterPin(14, LOW);
  writeRegisters();
} 
  delay(10);
}

void clearHourRegisters(){ //turns off all LED's in the hour Ring
  setRegisterPin(1 , HIGH);
  setRegisterPin(2 , HIGH);
  setRegisterPin(3 , HIGH);
  setRegisterPin(4 , HIGH);
  setRegisterPin(5 , HIGH);
  setRegisterPin(6 , HIGH);
  setRegisterPin(9 , HIGH);
  setRegisterPin(10 , HIGH);
  setRegisterPin(11 , HIGH);
  setRegisterPin(12 , HIGH);
  setRegisterPin(13 , HIGH);
  setRegisterPin(14 , HIGH);
  setRegisterPin(15 , HIGH);
  
  delay(10);
}
