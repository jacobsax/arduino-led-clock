void checkTime(){
  DateTime now = RTC.now();
  if (shouldBeMinute != now.minute()){
      Wire.beginTransmission(DS1307_I2C_ADDRESS);  // Open I2C line in write mode
 
   Wire.write((byte)0x00);                           // Set the register pointer to (0x00)
   Wire.write(decToBcd(13));               // Write seven bytes
   Wire.write(decToBcd(shouldBeMinute)); //minute
   Wire.write(decToBcd(now.hour()));  //hour    
   Wire.write(decToBcd(now.day())); //day of week
   Wire.write(decToBcd(00));//day of month
   Wire.write(decToBcd(now.month()));//month
   Wire.write(decToBcd(now.year())); //year
   Wire.endTransmission();                    // End write mode
  }
  
  if (shouldBeHour != now.hour()){
          Wire.beginTransmission(DS1307_I2C_ADDRESS);  // Open I2C line in write mode
 
   Wire.write((byte)0x00);                           // Set the register pointer to (0x00)
   Wire.write(decToBcd(13));               // Write seven bytes
   Wire.write(decToBcd(now.minute())); //minute
   Wire.write(decToBcd(shouldBeHour));  //hour    
   Wire.write(decToBcd(now.day())); //day of week
   Wire.write(decToBcd(00));//day of month
   Wire.write(decToBcd(now.month()));//month
   Wire.write(decToBcd(now.year())); //year
   Wire.endTransmission();                    // End write mode
  }
  
}
