void reprogramTime(){
  Wire.beginTransmission(DS1307_I2C_ADDRESS);  // Open I2C line in write mode
 
   Wire.write((byte)0x00);                           // Set the register pointer to (0x00)
   Wire.write(decToBcd(13));               // Write seven bytes
   Wire.write(decToBcd(48)); //minute
   Wire.write(decToBcd(17));  //hour    
   Wire.write(decToBcd(5)); //day of week
   Wire.write(decToBcd(18));//day of month
   Wire.write(decToBcd(10));//month
   Wire.write(decToBcd(2013)); //year
   Wire.endTransmission();                    // End write mode
}
